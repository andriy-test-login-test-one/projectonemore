@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>List of cars</h1></div>

                <div class="panel-body">

                    @if (count($errors->get_list))
                        <br>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->get_list->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('car.list')}}" method="get">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;">ID</th>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Year</th>
                                <th>Price sort</th>
                                <th>Price limit</th>
                            </tr>
                            <tr>
                                <th style="width: 100px;">
                                    <input type="radio" name="id" value="1"
                                           {{ (isset($Filters['id']) and $Filters['id'] == 1) ? "checked" : "" }}> Min->Max<br>
                                    <input type="radio" name="id" value="0"
                                            {{ (isset($Filters['id']) and $Filters['id'] == 0) ? "checked" : "" }}> Max->Min
                                </th>
                                <th>
                                    <input type="radio" name="brand" value="1"
                                            {{ (isset($Filters['brand']) and $Filters['brand'] == 1) ? "checked" : "" }}> A-Z (asc)<br>
                                    <input type="radio" name="brand" value="0"
                                            {{ (isset($Filters['brand']) and $Filters['brand'] == 0) ? "checked" : "" }}> Z-A</th>
                                <th>
                                    <input type="radio" name="model" value="1"
                                            {{ (isset($Filters['model']) and $Filters['model'] == 1) ? "checked" : "" }}> A-Z<br>
                                    <input type="radio" name="model" value="0"
                                            {{ (isset($Filters['model']) and $Filters['model'] == 0) ? "checked" : "" }}> Z-A
                                </th>
                                <th>
                                    <input type="radio" name="year" value="1"
                                            {{ (isset($Filters['year']) and $Filters['year'] == 1) ? "checked" : "" }}> Max->Min<br>
                                    <input type="radio" name="year" value="0"
                                            {{ (isset($Filters['year']) and $Filters['year'] == 0) ? "checked" : "" }}> Min->Max
                                </th>
                                <th>
                                    <input type="radio" name="price" value="1"
                                            {{ (isset($Filters['price']) and $Filters['price'] == 1) ? "checked" : "" }}> Min->Max<br>
                                    <input type="radio" name="price" value="0"
                                            {{ (isset($Filters['price']) and $Filters['price'] == 0) ? "checked" : "" }}> Max->Min
                                </th>
                                <th>
                                    <input type="number"  name="price_min" value="{{ isset($Filters['price_min']) ? $Filters['price_min'] : "0" }}"> Min price<br>
                                    <input type="number" name="price_max" value="{{ isset($Filters['price_max']) ? $Filters['price_max'] : "1000000" }}"> Max price
                                </th>
                            </tr>
                            <tr>
                                <th colspan="6">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <a href="{{route('car.list')}}"> <button class="btn btn-danger" type="button" >Clear Filter</button></a>
                                </th>
                            </tr>
                        </table>
                    </form>
                        <hr>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 50px;">ID</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Categoty (Year)</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        @if (count($cars['items'])>0)
                        @foreach ($cars['items'] as $car)
                        <tr>
                            <td>{{$car->id}}</td>
                            <td>{{$car->model->brand->name}}</td>
                            <td>{{$car->model->name}}</td>
                            <td>{{$car->cat->name. " (".$car->year.")"}}</td>
                            <td>{{$car->price}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                        {!! $cars['pagination']->appends(request()->input())->links('vendor.pagination.bootstrap-4') !!}
                </div>
                <hr>
                <h2>Cra create</h2>
                <div class="panel-body">
                    @if (count($errors->create))
                        <br>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->create->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('car.create')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <label class="next-label">Brands*</label>
                        <div class="next-input-wrapper">
                            <select class="form-control" id="brands" name="brand_id">
                                <option value=""></option>
                                @if (isset($brands['items']) && count($brands['items'])>0)
                                    @foreach ($brands['items'] as $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <label class="next-label">Models*</label>
                        <div class="next-input-wrapper">
                            <select class="form-control" id="models" name="model_id"></select>
                        </div>
                        <label class="next-label">Year*</label>
                        <div class="next-input-wrapper">
                            <input type="number" class="form-control" name="year">
                        </div>
                        <label class="next-label">Price*</label>
                        <div class="next-input-wrapper">
                            <input type="number" class="form-control" name="price">
                        </div>
                        <label class="next-label">Owner name*</label>
                        <div class="next-input-wrapper">
                            <input type="text" class="form-control" name="owner_name">
                        </div>
                        <br>
                        <button class="btn btn-success" type="submit">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
