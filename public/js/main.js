
$(document).ready(function() {

    $( "#brands" ).change(function() {

        var self = $(this),
            brand_id = self.val();

        // if (!brand_id)
        //     return;

        $.ajax({
            url: "/admin_ajax/brands/",
            type: 'GET',
            data: {brand_id: brand_id},
            dataType: "json",
            success: function (response) {
                var modelBlock = $("#models");
                modelBlock.html('');
                if (response.data.items.length>0) {
                    modelBlock.append("<option value=''></option>");
                    $.each(response.data.items, function( index, value ){
                        modelBlock.append("<option value='"+value.id+"'>"+value.name+"</option>");
                    });
                }
            }
        });
    });
});