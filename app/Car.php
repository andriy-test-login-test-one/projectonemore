<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Paginator;
use Illuminate\Support\Facades\DB;


class Car extends Model
{
    protected $table = 'cars';
    public $timestamps = false;

    public function model()
    {
        return $this->hasOne('App\Brand', 'id', 'model_id');
    }
    public function cat()
    {
        return $this->hasOne('App\Category', 'id', 'cat_id');
    }
    protected static function GetTypeSotr($boolean = 1) {
        if ($boolean == 1)
            return "ASC";
        return "DESC";
    }

    public static function getAll($page =1, $Filters=[])
    {
        $per_page=5;

        $data['items'] = Car::select(
            'cars.*',
            'models.name as model_name',
            'brands.name as brand_name')
            ->leftJoin('brands as models', function($join) {
                $join->on('cars.model_id', '=', 'models.id');
            })
            ->leftJoin('brands as brands' , function($join) {
                $join->on('models.parrent_id', '=', 'brands.id');
            });

        if (isset($Filters['price_min']) && isset($Filters['price_max']))
            $data['items'] = $data['items']
                ->whereBetween('cars.price',
                    [(int)$Filters['price_min'], (int)$Filters['price_max']]
                );


        if (count($Filters ) > 0) {

            foreach ($Filters as $key => $val) {

                switch ($key) {
                    case "id":
                        $data['items'] = $data['items']
                            ->orderBy('brands.id',self::GetTypeSotr($val));
                        break;
                    case "brand":
                        $data['items'] = $data['items']
                            ->orderBy('brands.name',self::GetTypeSotr($val));
                        break;
                    case "model":
                        $data['items'] = $data['items']
                            ->orderBy('models.name',self::GetTypeSotr($val));
                        break;
                    case "year":
                        $data['items'] = $data['items']
                            ->orderBy('cars.year',self::GetTypeSotr($val));
                        break;
                    case "price":
                        $data['items'] = $data['items']
                            ->orderBy('cars.price',self::GetTypeSotr($val));
                        break;
                }
            }


        }

        $data['items'] = $data['items']->skip($per_page * ($page - 1))
            ->limit($per_page)
            ->get();

        $data['count'] = Car::where('id', '>', 0)->count();

        $data['pagination'] =  new Paginator($data['items'], $data['count'],
            $per_page, $page, ['path'=>'/cars/']);

        return $data;
    }



    public static function createCar($Data)
    {
        //can use Transactions in the future
        $car = new Car;
        $car->model_id = $Data['model_id']; // has relations to brand
        $car->year = $Data['year'];
        $car->price = $Data['price'];
        $car->owner_name = $Data['owner_name'];
        $car->cat_id = Category::getCatIdByYear($Data['year']); // get category by year
        $car->save();
    }

}
