<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Brand;
use Illuminate\Support\Facades\Response;


class AdminAjaxModuleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return  json (list Models by brand)
     */
    public function listModels()
    {
        $brand_id = (int)Input::get('brand_id', 0);

        if (!empty($brand_id)) {
            $list_models = Brand::getAllModelsByBrandId($brand_id);

            return Response::json([
                    'status' => 'success',
                    'msg' => '200 OK',
                    'data' => $list_models
                ]
            );

        } else {
            return Response::json([
                    'status' => 'error',
                    'msg' => '400 Bad Request',
                    'data' => null,
                ]
            );
        }

    }
}
