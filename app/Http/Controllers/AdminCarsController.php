<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Brand;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class AdminCarsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * render page with all cars
     */
    public function index()
    {
        $page = Input::get('page', 1);
        $Filters=Input::get();

        $rules = [
            'id' => 'numeric|min:0|max:1',
            'brand' => 'numeric|min:0|max:1',
            'model' => 'numeric|min:0|max:1',
            'price' => 'numeric|min:0|max:1',
            'price_min' => 'numeric|min:0|max:1000000',
            'price_max' => 'numeric|min:0|max:1000000',
        ];
        $validator = Validator::make($Filters, $rules);


        if ($validator->fails())
            return redirect(route('car.list'))
                ->withErrors($validator, 'get_list');

        $cars = Car::getAll($page, $Filters);

        $brands = Brand::getAllBrands();//get brands(without mark..)

        if(view()->exists('cars')){
            return view('cars',[
                'cars' => $cars,
                'brands' => $brands,
                'Filters' => $Filters,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request)
    {
        if ($request->isMethod('POST')) {

            $Data = $request->except('_token');

            $rules = [
                'brand_id' => 'required|numeric|min:0',
                'model_id' => 'required|numeric|min:0',
                'year' => 'required|numeric|min:0|max:2155',
                'price' => 'required|numeric|min:0|max:1000000',
                'owner_name' => 'required|min:0|max:1000000',
            ];

            $validator = Validator::make($Data, $rules);

            if ($validator->fails())
                return redirect(route('car.list'))
                    ->withErrors($validator, 'create');

            Car::createCar($Data); //create cra and geneerate cat
            return redirect(route('car.list'));
        }

    }

    //
}
