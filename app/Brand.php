<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'parrent_id', 'id');
    }
    public static function getAllBrands()
    {
        return ['items' => Brand::where('parrent_id','0')->get()];
    }

    public static function getAllModelsByBrandId($brand_id)
    {
        return ['items' => Brand::where('parrent_id', $brand_id)->get()];
    }

}
