<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Paginator;


class Category extends Model
{
    protected $table = 'categories';

    public static function getCatIdByYear($year=0)
    {
        $cat = Category::where('from_year', '<=', $year)
            ->where('to_year', '>=', $year)
            ->first();
        return $cat->id;
    }

}
