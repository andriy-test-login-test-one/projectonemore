<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cars', 'AdminCarsController@index')->name('car.list');
Route::post('/cars', 'AdminCarsController@create')->name('car.create');
Route::get('/admin_ajax/brands/', 'AdminAjaxModuleController@listModels')->name('admin_ajax.module_list');
